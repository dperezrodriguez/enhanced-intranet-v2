(async function () {
    'use strict';
    const SERVICE_TABLE = true
    const CONSULTANT_ID = jQuery("#user-menu option[title*=mi_cuenta]").val().split("/")[2]
    const div = document.querySelector(".content-header")
    const custom_box = document.createElement("div")
    custom_box.innerHTML = `
        <div style="display:flex;">
            <div id="custom-div" style="margin:0 25px 15px; width: 80%;margin:20px 25px 15px;">
                <div style="display:flex; justify-content: space-between">
                <label for="date-selector">Select a date:
                    <select id="date-selector"></select>
                </label>
                <a href='https://intranet.divisadero.es/consultants/${CONSULTANT_ID}'>Reporte completo</a>
                <button id="reloadButton" style="background: none; border:none; cursor: pointer">
                    <?xml version="1.0" encoding="iso-8859-1"?>
                    <!-- Uploaded to: SVG Repo, www.svgrepo.com, Generator: SVG Repo Mixer Tools -->
                    <svg fill="#000000" height="20px" width="20px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                    viewBox="0 0 489.533 489.533" xml:space="preserve">
                    <g>
                        <path d="M268.175,488.161c98.2-11,176.9-89.5,188.1-187.7c14.7-128.4-85.1-237.7-210.2-239.1v-57.6c0-3.2-4-4.9-6.7-2.9
                        l-118.6,87.1c-2,1.5-2,4.4,0,5.9l118.6,87.1c2.7,2,6.7,0.2,6.7-2.9v-57.5c87.9,1.4,158.3,76.2,152.3,165.6
                        c-5.1,76.9-67.8,139.3-144.7,144.2c-81.5,5.2-150.8-53-163.2-130c-2.3-14.3-14.8-24.7-29.2-24.7c-17.9,0-31.9,15.9-29.1,33.6
                        C49.575,418.961,150.875,501.261,268.175,488.161z"/>
                    </g>
                    </svg>
                </button>
                </div>

                <table id="table" class="main-table">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Account</th>
                            <th>Service</th>
                            <th>Work</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody id="table-body"></tbody>
                </table>
            </div>
            <div class="calendar" style="margin:25px;"></div>
        </div>
     `
    div.append(custom_box)

    const iframe = document.createElement("iframe")
    iframe.src = "https://intranet.divisadero.es/consultants/" + CONSULTANT_ID
    iframe.style = "height: 1px; width: 1px; border: none"



    div.prepend(iframe)

    window.customDataObject = {}

    function waitForElm(selector) {
        return new Promise(resolve => {
            if (document.querySelector(selector)) {
                return resolve(document.querySelector(selector));
            }

            const observer = new MutationObserver(mutations => {
                if (document.querySelector(selector)) {
                    resolve(document.querySelector(selector));
                    observer.disconnect();
                }
            });

            observer.observe(document.body, {
                childList: true,
                subtree: true
            });
        });
    }

    function reloadIframe() {
        document.querySelector("#reloadButton").addEventListener("click", () => {
            window.customDataObject = {}
            document.querySelector(".div-container").remove()
            document.querySelector("#service_table").remove()
            document.querySelector("iframe").contentWindow.location.reload();
        })

        document.querySelectorAll(".update-tasks").forEach(async el => {
            el.addEventListener("click", async () => {
                await waitForElm(".fancybox-wrap #ticket_submit")
                document.querySelectorAll("#ticket_submit").forEach(el => {
                    el.addEventListener("click", () => {
                        setTimeout(() => {
                            window.customDataObject = {}
                            document.querySelector(".div-container").remove()
                            document.querySelector("#service_table").remove()
                            document.querySelector("iframe").contentWindow.location.reload();
                        }, 2000)

                    })
                })
            })
        })
    }

    function groupByServiceWithTimeSum(data) {
        let result = {};
        for (const date in data) {
            for (const entry of data[date]) {
                const service = entry.service;
                if (!result[service]) {
                    result[service] = {
                        time: 0,
                        entries: []
                    };
                }
                result[service].time += entry.time;
                result[service].entries.push({
                    date: date,
                    account: entry.account,
                    work: entry.work,
                    time: entry.time
                });
            }
        }
        return result;
    }

    function generateServiceTable(data) {
        let html = `
            <table id="service_table" style="background: lightblue" class="main-table">
                <tr style=" text-align: center;">
                    <td colspan="2" style="  background: #4d4d4d; color: #fff;">Servicios</td>
                </tr>`;
        let i = 0;
        let sumHours = 0
        for (let service in data) {
            let totalTime = Math.floor(data[service].time / 60);
            let totalMinutes = (data[service].time % 60) < 10 ? `0${data[service].time % 60}` : data[service].time % 60

            let oddOrEven = i % 2 === 0 ? "even" : "odd";
            html += `<tr class="${oddOrEven}"><td>${service}</td><td>${totalTime}:${totalMinutes}</td></tr>`;
            i++
            sumHours += data[service].time
        }
        sumHours = `${Math.floor(sumHours / 60)}:${sumHours % 60 < 10 ? `0${sumHours % 60}` : sumHours % 60}`
        html += `
            <tr style="background: #4d4d4d; color: #fff !important; font-weight: bold !important">
                <td style="background: #4d4d4d; color: #fff !important; font-weight: bold !important">Total</td>
                <td style="background: #4d4d4d; color: #fff !important; font-weight: bold !important">${sumHours}</td>
            </tr>`;

        html += '</table>';
        return html;
    }

    reloadIframe()

    document.querySelector("iframe").addEventListener("load", async function () {
        let currentDate = ""
        const elements = this.contentDocument.querySelectorAll("#dedication_day_table tr");
        const tableBody = document.getElementById("table-body");
        const dateSelector = document.getElementById("date-selector");

        function tableToObject() {
            for (let i = 0; i < elements.length; i++) {
                const el = elements[i];
                if (el.getAttribute("class") && !el.getAttribute("class").includes("service_details") && el.querySelector("td").innerText !== "") {
                    window.parent.customDataObject[el.querySelector("td").innerText] = [];
                    currentDate = el.querySelector("td").innerText;
                } else if (el.getAttribute("class") && el.getAttribute("class").includes("service_details")) {
                    window.parent.customDataObject[currentDate].push({
                        account: el.querySelector("td:nth-child(2)").innerText,
                        service: el.querySelector("td:nth-child(3)").innerText,
                        work: el.querySelector("td:nth-child(4)").innerText.substr("11"),
                        time: Math.round(parseFloat(el.querySelector("td:nth-child(5)").innerText) * 60),
                    });
                }
            }
        }

        function populateCustomSumObject() {
            const year = new Date(Date.now()).getFullYear();
            const month = new Date(Date.now()).getMonth();

            const daysInMonth = new Date(year, month + 1, 0).getDate();
            const dates = [];
            for (let i = 1; i <= daysInMonth; i++) {
                const day = ('0' + i).slice(-2);
                const formattedMonth = ('0' + (month + 1)).slice(-2);
                const formattedDate = `${day}/${formattedMonth}/${year}`;
                dates.push(formattedDate);
            }

            window.customDataObjectDailySum = dates.reduce((acc, date) => {
                acc[date] = customDataObject[date] ? customDataObject[date].reduce((sum, entry) => sum + entry.time, 0) : 0;
                return acc;
            }, {});
        }

        function populateDateSelector() {
            dateSelector.innerHTML = ""
            for (const date in window.customDataObject) {
                const option = document.createElement("option");
                option.value = date;
                option.text = date;
                dateSelector.appendChild(option);
            }
        }

        function generateCalendar() {
            const style = document.createElement('style');
            style.innerHTML = `
                .calendar-table {
                width: 100%;
                border-collapse: collapse;
                display: flex;
                flex-wrap: wrap;
                }

                .calendar-table .calendarCell {
                border: 1px solid #ddd;
                text-align: center;
                height: 24px;
                padding: 5px 0px;
                width: 13%;
                }

                .calendar-table th {
                background-color: #ddd;
                }


                .calendar-table .calendarCell {
                background-color: #eee;
                }

                .div-container {
                width: 300px
                }

                .calendar-table .header {
                background-color: #4d4d4d;
                color: #fff
                }

                .calendar-table .title {
                flex: 1 100%;
                margin-right: 13px;
                background-color: #4d4d4d;
                color: #fff
                }

                .weekend{
                background-color: grey !important;
                }

                .success{
                background-color: #e0f0e0 !important;
                }
            `;

            document.head.appendChild(style);

            const year = new Date(Date.now()).getFullYear();
            const month = new Date(Date.now()).getMonth();
            const monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];

            const daysInMonth = new Date(year, month + 1, 0).getDate();
            const dates = [];
            for (let i = 1; i <= daysInMonth; i++) {
                const day = ('0' + i).slice(-2);
                const formattedMonth = ('0' + (month + 1)).slice(-2);
                const formattedDate = `${day}/${formattedMonth}/${year}`;
                dates.push(formattedDate);
            }
            const divContainer = document.createElement("div")
            divContainer.className = "div-container"
            const table = document.createElement("div");
            table.className = "calendar-table"
            const header_row = document.createElement("div")
            table.innerHTML = `
                <div class="calendarCell title">${monthNames[month]}</div>
                <div class="calendarCell header">L</div>
                <div class="calendarCell header">M</div>
                <div class="calendarCell header">X</div>
                <div class="calendarCell header">J</div>
                <div class="calendarCell header">V</div>
                <div class="calendarCell header">S</div>
                <div class="calendarCell header">D</div>
            `
            table.append(header_row)

            for (let i = 1; i <= daysInMonth; i++) {
                const day = ('0' + i).slice(-2);
                const formattedMonth = ('0' + (month + 1)).slice(-2);
                const formattedDate = `${day}/${formattedMonth}/${year}`;
                const date = new Date(`${formattedMonth}-${day}-${year}`)
                const hourMinutes = `${Math.floor(customDataObjectDailySum[formattedDate] / 60)}:${('0' + (customDataObjectDailySum[formattedDate] % 60)).slice(-2)}` || 0;


                const row = document.createElement("div");
                row.className = "calendarCell"
                row.id = date
                if (date.getDay() === 6 || date.getDay() === 0) {
                    row.className += " weekend"
                }

                if (
                    (date.getDay() === 5 && customDataObjectDailySum[formattedDate] >= 420) ||
                    (date.getDay() !== 5 && date.getDay() !== 6 && date.getDay() !== 0 && customDataObjectDailySum[formattedDate] >= 495)
                ) {
                    row.className += " success"
                }

                if (i === 1 && date != 0) {
                    const missingCells = date.getDay() === 0 ? 6 : date.getDay() - 1
                    for (let i = 0; i < Array(missingCells).length; i++) {
                        const row = document.createElement("div");
                        row.className = "calendarCell"
                        row.className += " weekend"
                        table.append(row)
                    }
                }

                const valueCell = document.createElement("div");
                valueCell.classList = "calendarValue"
                valueCell.textContent = hourMinutes === "0:00" ? "" : hourMinutes
                row.appendChild(valueCell);

                table.appendChild(row);
            }
            divContainer.append(table)
            document.querySelector(".calendar").appendChild(divContainer);
        }

        function populateTable(selectedDate) {
            tableBody.innerHTML = "";
            let totalMinutes = 0
            for (const date in window.customDataObject) {
                if (!selectedDate || date === selectedDate) {
                    let loopNum = 0
                    window.customDataObject[date].forEach(entry => {
                        const row = document.createElement("tr");
                        row.className = loopNum % 2 == 0 ? "even" : "odd"
                        row.innerHTML = `
                            <td>${date}</td>
                            <td>${entry.account}</td>
                            <td>${entry.service}</td>
                            <td>${entry.work}</td>
                            <td>${entry.time}</td>
                        `;
                        totalMinutes += entry.time
                        tableBody.appendChild(row);
                        loopNum++
                    });
                }
            }

            const row = document.createElement("tr")
            row.style = "background: #4d4d4d; color: #fff !important; font-weight: bold !important"
            row.innerHTML = `
                <td style="background: #4d4d4d; color: #fff !important; font-weight: bold !important">Total</td>
                <td></td>
                <td></td>
                <td></td>
                <td style="background: #4d4d4d; color: #fff !important; font-weight: bold !important">${Math.floor(totalMinutes * 100) / 100}</td>
            `;
            tableBody.appendChild(row);
        }

        dateSelector.addEventListener("change", () => {
            const selectedDate = dateSelector.value;
            populateTable(selectedDate);
        });

        tableToObject()
        populateDateSelector();
        const lastDate = Object.keys(customDataObject)[Object.keys(customDataObject).length - 1]
        populateTable(lastDate);
        dateSelector.value = lastDate
        populateCustomSumObject()
        generateCalendar()
        window.groupByServiceWithTimeSum = groupByServiceWithTimeSum(window.customDataObject)

        if (SERVICE_TABLE) {
            const div_content = document.querySelector(".content-header > div > div")
            const serviceBox = document.createElement("div")
            serviceBox.innerHTML = generateServiceTable(window.groupByServiceWithTimeSum)
            div_content.appendChild(serviceBox)
        }
    })
})();